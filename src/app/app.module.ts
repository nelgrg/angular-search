import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BlogSearchComponent } from 'src/app/blog-search/blog-search.component';

import { PagerService } from 'src/app/services/pagination.service';
import { HighlightSearchPipe } from 'src/app/pipes/search.pipe';  

/** material design imports **/
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
/** material design imports **/

@NgModule({
  declarations: [
    AppComponent,
    BlogSearchComponent,
    HighlightSearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
  ],
  providers: [
    PagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
