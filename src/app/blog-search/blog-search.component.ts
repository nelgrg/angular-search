import { Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-blog-search',
    templateUrl: './blog-search.component.html',
    styleUrls: ['./blog-search.component.scss']
})

export class BlogSearchComponent {
    @Output() searchcriteria = new EventEmitter<String>();
    searchword: string;

    constructor() { }

    searchThis() {
        this.searchcriteria.emit(this.searchword); // emits searchword on every key press
    }
}