import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';

export interface IBlogPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  blogUrl: string = 'https://jsonplaceholder.typicode.com/posts'; //used fake API url

  constructor(private http: HttpClient) { }

  getBlogPosts(): Observable<IBlogPost[]> {
    return this.http.get<IBlogPost[]>(this.blogUrl);
  }
}
