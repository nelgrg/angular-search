import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/services/blog.service';
import { PagerService } from 'src/app/services/pagination.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  posts = [];
  tempPost = [];

  search: string = "";

  // pager object
  pager: any = {};

  // paged items
  pagedItems: any[];

  constructor(private blogService: BlogService, private pagerService: PagerService) { }

  ngOnInit(): void {
    this.blogService.getBlogPosts().subscribe(data => {
      this.posts = data;
      this.tempPost = data;
      this.setPage(1);
    });
  }

  searchThis(data) {
    if (data) {
      this.search = data;
      this.posts = this.tempPost.filter(function (ele) {
        let arrayelement = `${ele.title.toLowerCase()} ${ele.body.toLowerCase()}`;
        return arrayelement.includes(data); // checks for matching data on both body and title
      })

    }
    else {
      this.search = "";
      this.pagedItems = this.tempPost;
    }
    this.setPage(this.pager.currentPage);
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.posts.length, page);

    // get current page of items
    this.pagedItems = this.posts.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
